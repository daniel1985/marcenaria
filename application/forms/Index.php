<?php

class Application_Form_Index extends Zend_Form {

    public function init() {

        $this->addElementPrefixPath('Numenor_Form_Decorator', 'Numenor/Form/Decorator/', 'decorator');
        $this->setAttrib('id', 'form');
        $this->setLegend('Cadastro de usuario');
        $this->setAttrib('class', 'form-horizontal');


        $usuario = new Zend_Form_Element_Text('usuario');
        $usuario->setLabel('Usuário:')
            ->setRequired(true)
            ->setValidators(array('NotEmpty'))
            ->setOptions(array(
                'attr_label' => array(
                    'class' => 'nome'
                ),
                'attr_campo' => array(
                    'class' => 'input-xlarge', //,
                )
            ));

        $senha = new Zend_Form_Element_Text('email');
        $senha->setLabel('Senha:')
            ->setRequired(false)
            ->setValidators(array('NotEmpty'))
            ->setOptions(array(
                'attr_linha_form' => array(
                    'class' => 'senha'
                ),
                'attr_label' => array(
                    'class' => 'senha'
                ),
                'attr_campo' => array(
                    'class' => 'input-xlarge', //,
                )
            ));


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Entrar')
            ->setOptions(array(
                'attr_linha_form' => array(
                    'class' => 'form-actions cancelar '
                ),
                'attr_campo' => array(
                    'class' => 'btn btn-success'
                )
            ));

        $this->addElements(array(
            $usuario, $senha, $submit

        ));


        // Define a existencia de uma div envolvendo as linhas do formulário.
        $this->setDecorators(array(
            'FormElements',
            array(
                array(
                    'data' => 'HtmlTag'
                ),
                array(
                    'tag' => 'div',
                    'class' => ''
                )
            ),
            'Form'
        ));

        // Sobrescreve decorators existentes com os customizados.
        $this->setElementDecorators(array('Composite'));
    }

}
